# Baidu Translator

---

# Introduction 

> A web server wrapped [Baidu Translator API](http://api.fanyi.baidu.com/api/trans/vip/translate) based on spring boot.

---

# Usage

1. Clone project: `git clone https://git.oschina.net/ube/baidu-translator.git`;
1. Run project: `mvn clean spring-boot:run -Drun.jvmArguments="-Dbaidu.appid={appid} -Dbaidu.token={token} -Dbaidu.limit={limit}" -Dbaidu.from={from} -Dbaidu.to={to}`

> Where `{appid}` means your application id required from [Baidu Translator](http://api.fanyi.baidu.com/api/trans/product/index),
> you can find `{token}` after your requirements allowed. You can specify `{limit}` as what you want
> to limit characters totally translated by server. This command will simply translate text from `{from}` to `{to}`.
> Both `{from}` and `{to}` are two characters according to [document](http://api.fanyi.baidu.com/api/trans/product/apidoc) from official site. 