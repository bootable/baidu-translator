package net.oschina.soiff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoiffApplication {

        public static void main(String[] args) {
                SpringApplication.run(SoiffApplication.class, args);
        }
}

